package edu.dnu.fpm.pz;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.nio.file.FileSystems;

@Command(
        name = "lookdir",
        description = "Look for subdirectory in directory"
)
public class Main implements Runnable {
    @Option(names = {"-p", "--path"}, description = "Path of directory for looking for")
    private String path = FileSystems.getDefault().getPath("").toAbsolutePath().toString();
    @Option(names = {"-n", "--name"}, description = "Name of directory for finding", required = true)
    private String name;
    @Option(names = {"-h", "--help"}, description = "Info about command")
    private Boolean help;

    public static int foundDirectories = 0;

    public static void main(String[] args) {
        new CommandLine(new Main()).execute(args);

        if (foundDirectories == 0) throw new RuntimeException("1");
    }

    @Override
    public void run() {
        if (help != null) {
            System.out.println("Usage: lookdir [-h] -n=<name> [-p=<path>]\n" +
                    "Look for subdirectory in directory\n" +
                    "  -h, --help          Info about command\n" +
                    "  -n, --name=<name>   Name of directory for finding\n" +
                    "  -p, --path=<path>   Path of directory for looking for");
        } else {
            lookForDirectoryByName(name, new File(path));
        }
    }

    private void lookForDirectoryByName(String name, File file) {
        if (file.isFile()) return;

        for (File subFile : file.listFiles()) {
            if (subFile.isDirectory()) {
                if (subFile.getName().matches(name)) {
                    System.out.println(subFile.getAbsolutePath());
                    foundDirectories++;
                }

                lookForDirectoryByName(name, subFile);
            }
        }
    }
}
